﻿# 视频链接

## 阿丽塔: 战斗天使

* [【阿丽塔-战斗天使】4K画质超燃踩点混剪 - ZLOED](https://www.bilibili.com/video/av44575553?from=search&seid=16872108667518843355)
* [【阿丽塔-战斗天使】感受那份优雅的暴力 - 林肯电影](https://www.bilibili.com/video/av44765551?from=search&seid=14629641729182428759)
* [【阿丽塔-战斗天使】超燃混剪【1080P】- Sakiyaki](https://www.bilibili.com/video/av44032489?from=search&seid=3889842535154808136)

## 娱乐

* [虎牙视频 - 狼人杀GodLie](http://v.huya.com/u/1560182536/video.html)
* [【GodLie S3】第五期第二局：史上爆笑狼人导师评审团上线！](https://v.huya.com/play/118471061.html)
* [狼人杀GODLIE《载入史册经典局系列》02梦魇圈圈演技炸裂一狼屠城](https://www.bilibili.com/video/av498314391/)
* [爱奇艺 - 梦想改造家](https://so.iqiyi.com/so/q_%E6%A2%A6%E6%83%B3%E6%94%B9%E9%80%A0%E5%AE%B6)
* [bilibili 哔哩哔哩 - 胥渡吧](https://space.bilibili.com/8112659/video)
* [胥渡吧 - 明清帝王对话](https://space.bilibili.com/8112659/channel/detail?cid=134448)
* [bilibili 哔哩哔哩 - 四郎讲棋](https://space.bilibili.com/291377718/video)
* [bilibili 哔哩哔哩 - 那年那兔那些事儿](https://www.bilibili.com/bangumi/play/ss1689/)
* [2022年世界杯_央视网(cctv.com)](https://worldcup.cctv.com/2022/schedule/index.shtml)
* [县委大院](https://tv.cctv.com/2022/12/07/VIDA8Vd2rrbQ4Hw0bbS26BeY221207.shtml)

## 游戏

* [英雄联盟 - LPL赛区官网比赛视频地址](https://lpl.qq.com/es/video.shtml)
* [虎牙视频 - 英雄联盟 - LCK冠军联赛](http://v.huya.com/u/1616443012)
* [英雄联盟 - 2019年全球总决赛](https://lpl.qq.com/es/worlds/2019/index.html)
* [英雄联盟 - 2019全明星赛](https://lpl.qq.com/act/a20191127allstar/index.html)
* [英雄联盟 - 2020全球总决赛 - 直播](https://lpl.qq.com/es/live.shtml)
* [英雄联盟 - 2020全球总决赛 - 官网](https://lpl.qq.com/es/worlds/2020/)
* [英雄联盟 - 2023全球总决赛 - B站视频列表](https://space.bilibili.com/50329118/channel/seriesdetail?sid=3681623)

### 狼人杀

* [【Godlie 第二季】第十期 第一局 最强狼队上线](https://v.huya.com/play/63489785.html)

## 美剧

* 神盾局特工 - 系列
* 吸血鬼日记 - 系列
* 性教育
* [猎鹰与冬兵 - 闪电影视](https://www.ak1080.com/voddetail/449390.html)
* [洛基](https://www.jingguanghang.com/p/41310/index_1_1.html)

## 综艺

#### 心动的信号 第二季

本想用工作抑制的爱情的渴望被冲毁殆尽......

![](./img/心动的信号01.jpg)
![](./img/心动的信号02.jpg)

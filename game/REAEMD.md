﻿# 游戏

## 口袋妖怪

* [漆黑的魅影5.0攻略](https://www.3dmgame.com/gl/3833381.html)
* [口袋妖怪漆黑的魅影5.0二周目图文完整流程攻略](https://www.wyaq.com/youxi/gonglue/5803.html)

## 天天象棋

* [天天象棋闯关模式攻略动态图解 - 4399手机游戏网](http://news.4399.com/gonglue/ttxq/cgms/)

## 想法

### 关于队友挂机

使用机器人暂时代打, 如果再游戏中系统检测到挂机行为, 发起投票(类似于投降投票), 都同意时, 使用机器人来待打, 待打为了不送人头可以守家,清兵线插眼等行为, 当然为了防止恶意使用机器代打行为, 需要记录黑名单, 累计次数封号, 暂停一定时间才可以开局等等, 判断是否挂机也可使用队友投票


## GBA 游戏

* [回忆杀，小霸王游戏机“红白机游戏500合1”，经典的小黄卡，是否勾起了你的童年记忆 ](https://www.52pojie.cn/thread-609586-1-1.html)
* [再来一波回忆杀，GBA游戏大合集（212作+365作），整理了一下分享给大家](https://www.52pojie.cn/thread-609612-1-1.html)


## 麻将

### 学霸公式:

```
n*AAA + m*ABC + i*DD = 胡牌

m/n>=0, i>=1
```

AAA就是三个一样的牌，ABC就是顺子，DD就是对子。mn可以为0，这句话得用高中数学来理解：mm中可以至少有1个为0。

**注意**：东南西北中发白这7张牌不能组成ABC的形式，只能组成AAA。

### 学习链接

* [麻将怎么打(新手入门篇) - 百度经验](https://jingyan.baidu.com/article/219f4bf79469c3de442d38ba.html)
* [如何学会打麻将? - 知乎](https://www.zhihu.com/question/20327419)

* [扫雷游戏, 还有很多很好玩的小游戏](https://www.saolei.org.cn/)
* [免费数独](http://cn.sudokupuzzle.org/)
* [扫雷游戏, 还有很多很好玩的小游戏](https://www.saolei123.com/)
* [迷宫](https://www.saolei123.com/game/migong/)
* [围住小猫](https://ganlvtech.github.io/phaser-catch-the-cat/)
* [可自定义的扫雷游戏](https://www.weizhang.site/Minesweeper/index.html)
* [LOL - 完成训练的三个阶段，你必成为走A大神！](https://lol.qq.com/news/detail.shtml?type=6&docid=5515373774544295280)
* [风灵月影 - 修改器 - 黑神话悟空](https://flingtrainer.com/trainer/black-myth-wukong-trainer/)

## Steam 待游戏列表

* Stardew Valley - Steam版牧场物语
* unepic
* [Watt Toolkit (Steam 加速器)](https://gitee.com/rmbgame/SteamTools)
